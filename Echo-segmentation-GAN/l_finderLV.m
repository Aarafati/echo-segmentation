
for ii=1:29%%:15
    figure;
    lv(:,:)=gt_mask_ED(ii,:,:,1);
    %imshow(lv,[0 1])
    %hold on
    points=contour(lv,1);
    points(points < 20)=mean(points(1,:));
    points(points > 240)=mean(points(2,:));
    left= min(points(1,:));right=max(points(1,:));
    x=floor(mean([left,right]));
    ind=find((points(1,:)-x)<=1);
    y=max(points(2,ind));
    point=[x,y];
    plot(x,y,'*')
    distances = sqrt((points(1,:)-x).^2 + (points(2,:)-y).^2);
    [maxDistancegtED(ii), indexOfMax] = max(distances);
    plot(points(1,indexOfMax),points(2,indexOfMax),'*')

    lengtED(ii)=sqrt(lgtED(ii,1)^2+lgtED(ii,2)^2);
    clear points ind 
end
figure;
bar(maxDistancegtED-lengtED)