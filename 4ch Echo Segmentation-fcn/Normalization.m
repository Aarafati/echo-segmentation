%% train Data
cd ('./Training')
labellist = dir(fullfile(pwd,'Labels','*.mat'));
imglist=dir(fullfile(pwd,'Images','*.mat'));
n=numel(imglist);
addpath('./Labels');
addpath('./Images');
resz=[315 215];
for cnt=1:n
    cnt
    image=open(imglist(cnt).name);
    LV=open(labellist(cnt).name);
    
    Images=image.LVImgc;
    Images=double(Images);
    Images=imresize(Images,'OutputSize',resz);
    Images_res(:,:,cnt)=Images;
    
    Labels=LV.LVmaskc;
    Labels=double(Labels);
    Labels=imresize(Labels,'OutputSize',resz);
    Labels_res(:,:,cnt)=Labels;
    close all
end 
% SD=std(double(Images(:)));
patches=reshape(Images_res,[resz(1)*resz(2),n]);
meanPatches=mean(patches,2);
patches = bsxfun(@minus, patches, meanPatches);
pstd = 3 * std(patches(:));
patches = max(min(patches, pstd), -pstd) / pstd;
patches = (patches + 1) * 0.4 + 0.1;