clear all
addpath ('C:\Users\Arghavan\Desktop\altmany-export_fig-e1b8666');


filename='64403546';
ES=30; 
ED=19;
I=dicomread(filename);
size=size(I);
% n=floor(size(4)/5);

for ii=1:2
    if ii==1
        t=ED;
    end
    if ii==2
        t=ES;
    end
    image=I(:,:,1,t);
    fig=imshow(image);
%     %LV
    lv=imfreehand(gca,'closed',false);
    setColor(lv,'y'); 
    LVmask=createMask(lv);
    labellv_xy=contourc(double(LVmask));
    
    lblIDlv=['LVmouse', '.txt'];
    lblIDlvmat=['LVmouse', '.mat'];
    save(lblIDlvmat,'LVmask');
    if ii==1
        lblIDlvmatED=['LV',filename, num2str(t),'ED', '.mat'];
        save(lblIDlvmatED,'LVmask');
    else
        lblIDlvmatES=['LV',filename, num2str(t),'ES', '.mat'];
        save(lblIDlvmatES,'LVmask');
    end
    f =fopen(lblIDlv,'wt');
    fprintf (f,'%f  %f  \n',labellv_xy);
    fclose('all');
    % RV
    rv=imfreehand(gca,'closed',false);
    setColor(rv,'r'); 
    RVmask=createMask(rv);
    labelrv_xy=contourc(double(RVmask));
    
    lblIDrv=['RV',filename, num2str(t), '.txt'];
    lblIDrvmat=['RV',filename, num2str(t), '.mat'];
    save(lblIDrvmat,'RVmask');
    ff=fopen(lblIDrv,'wt');
    fprintf (ff,'%f  %f   \n',labelrv_xy);
    fclose('all');
     if ii==1
        lblIDrvmatED=['RV',filename, num2str(t),'ED', '.mat'];
        save(lblIDrvmatED,'RVmask');
    else
        lblIDrvmatES=['RV',filename, num2str(t),'ES', '.mat'];
        save(lblIDrvmatES,'RVmask');
    end
    %LA
    la=imfreehand(gca,'closed',false);
    setColor(la,'b'); 
    LAmask=createMask(la);
    labella_xy=contourc(double(LAmask));
    
    lblIDla=['LA',filename, num2str(t), '.txt'];
    lblIDlamat=['LA',filename, num2str(t), '.mat'];
    save(lblIDlamat,'LAmask');
    fff =fopen(lblIDla,'wt');
    fprintf (fff,'%f  %f  \n',labella_xy);
    fclose('all');
    if ii==1
        lblIDlamatED=['LA',filename, num2str(t),'ED', '.mat'];
        save(lblIDlamatED,'LAmask');
    else
        lblIDlamatES=['LA',filename, num2str(t),'ES', '.mat'];
        save(lblIDlamatES,'LAmask');
    end
    %RA
    ra=imfreehand(gca,'closed',false);
    setColor(ra,'m'); 
    RAmask=createMask(ra);
    labelra_xy=contourc(double(RAmask));
    
    lblIDra=['RA',filename, num2str(t), '.txt'];
    lblIDramat=['RA',filename, num2str(t), '.mat'];
    save(lblIDramat,'RAmask');
    ffff=fopen(lblIDra,'wt');
    fprintf (ffff,'%f  %f  \n',labelra_xy);
    fclose('all');
    if ii==1
        lblIDramatED=['RA',filename, num2str(t),'ED', '.mat'];
        save(lblIDramatED,'RAmask');
    else
        lblIDramatES=['RA',filename, num2str(t),'ES', '.mat'];
        save(lblIDramatES,'RAmask');
    end
    ImageID=['Img',filename, num2str(t),'.mat'];
    save(ImageID,'image');
    
  
   export_fig(['Img',filename, num2str(t),'A'])
   if ii==1
   ImageID=['Img',filename, num2str(t),'ED','.mat'];
    save(ImageID,'image');
    
  
   export_fig(['Img',filename, num2str(t),'ED','A'])
   else
       ImageID=['Img',filename, num2str(t),'ES','.mat'];
    save(ImageID,'image');
    
  
   export_fig(['Img',filename, num2str(t),'ES','A'])
   end
    clear labelrv_xy labellv_xy RVmask LVmask ;
end

 