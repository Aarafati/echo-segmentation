import caffe
#import surgery, score

import numpy as np
import os
import sys

try:
    import setproctitle
    setproctitle.setproctitle(os.path.basename(os.getcwd()))
except:
    pass

weights = '/media/exx/22d9dd91-2bea-4cea-b04a-647943fbfc73/caffe-examples/semanticseg/fcn32s-heavy-pascal.caffemodel'
#weights = '/media/exx/22d9dd91-2bea-4cea-b04a-647943fbfc73/caffe-examples/trial/pascalsemanticseg_LV_256_allpng_Nest_iter_2000.caffemodel'
#weights='/media/exx/22d9dd91-2bea-4cea-b04a-647943fbfc73/caffe-examples/u-net-release/phseg_v5.caffemodel'
# init
#caffe.set_device(int(sys.argv[1]))
caffe.set_mode_gpu()
solver = caffe.SGDSolver('/media/exx/22d9dd91-2bea-4cea-b04a-647943fbfc73/caffe-examples/semanticseg/newsolver.prototxt')
solver.net.copy_from(weights)
solver.solve()
#solver.net.copy_from(weights)

# surgeries
#interp_layers = [k for k in solver.net.params.keys() if 'up' in k]
#surgery.interp(solver.net, interp_layers)

# scoring
#val = np.loadtxt('../data/pascal/VOC2010/ImageSets/Main/val.txt', dtype=str)

#for _ in range(900):
#    solver.step(8000)
#    score.seg_tests(solver, False, val, layer='score')
#solver = caffe.SGDSolver(prototxt_solver)
 
iterations = 1000 # Depending on dataset size, batch size etc. ...
for iteration in range(iterations):
    solver.step(1) # We could also do larger steps (i.e. multiple iterations at once).
