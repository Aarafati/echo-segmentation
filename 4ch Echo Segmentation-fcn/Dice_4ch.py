def Dice_4ch(auto,manual):

    for k in range(1, 5):
        autoseg = np.sign(-1 * ((np.array(auto) - k) * (np.array(auto) - k))) + 1
        manseg = np.sign(-1 * ((np.array(manual) - k) * (np.array(manual) - k))) + 1
        dice[k - 1] = np.sum(np.array(autoseg) * np.array(manseg)) * 2.0 / (np.sum(autoseg) + np.sum(manseg))
    return dice