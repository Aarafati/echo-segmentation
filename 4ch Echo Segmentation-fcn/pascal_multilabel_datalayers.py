# imports
import json
import time
import pickle
import scipy.misc
import skimage.io
import caffe
import matplotlib.pyplot as plt
import numpy as np
import os.path as osp

from xml.dom import minidom
from random import shuffle
from threading import Thread
from PIL import Image

from tools import SimpleTransformer
#print('begin')
h = 256;
w = 256;
class PascalMultilabelDataLayerSync(caffe.Layer):

    """
    This is a simple synchronous datalayer for training a multilabel model on
    PASCAL.
    """

    def setup(self, bottom, top):

        self.top_names = ['data', 'label']

        # === Read input parameters ===

        # params is a python dictionary with layer parameters.
        params = eval(self.param_str)
        augmentation_params = eval(self.param_str)
        #dataaugparams=eval(self.augmentation_params_str)
        #print('setup')
        # Check the parameters for validity.
        check_params(params)

        # store input as class variables
        self.batch_size = params['batch_size']

        # Create a batch loader to load the images.
        self.batch_loader = BatchLoader(params,augmentation_params, None)

        params['im_shape']= [h,w]
        # === reshape tops ===
        # since we use a fixed input image size, we can shape the data layer
        # once. Else, we'd have to do it in the reshape call.
        top[0].reshape(
            self.batch_size, 3, params['im_shape'][0], params['im_shape'][1])
        # Note the 20 channels (because PASCAL has 20 classes.)
        top[1].reshape(self.batch_size, params['im_shape'][0], params['im_shape'][1])

        print_info("PascalMultilabelDataLayerSync", params)
        return augmentation_params
    def forward(self, bottom, top):
        """
        Load data.
        """
        augmentation_params = eval(self.param_str)
        tform_ds_8x = build_ds_transform(1.0, target_size=(256 , 256))
        tform_ds_cropped33 = build_ds_transform(2.0, target_size=(256, 256))
        #tform_ds_cc = build_ds_transform(1.0, target_size=(53, 53))

        #tform_identity = skimage.transform.AffineTransform()  # this is an identity transform by default

        ds_transforms = [tform_ds_cropped33, tform_ds_8x]
        for itt in range(self.batch_size):
            # Use the batch loader to load the next image.
            im, lbll = self.batch_loader.load_next_image(ds_transforms,augmentation_params)
            #print(itt)
            #print('forward')
            # Add directly to the caffe data layer
            top[0].data[itt, ...] = im
            top[1].data[itt, ...] = lbll

    def reshape(self, bottom, top):
        """
        There is no need to reshape the data, since the input is of fixed size
        (rows and columns)
        """
        pass

    def backward(self, top, propagate_down, bottom):
        """
        These layers does not back propagate
        """
        pass


class BatchLoader(object):

    """
    This class abstracts away the loading of images.
    Images can either be loaded singly, or in a batch. The latter is used for
    the asyncronous data layer to preload batches while other processing is
    performed.
    """
    def __init__(self, params, augmentation_params,result):
        self.result = result
        self.batch_size = params['batch_size']
        self.pascal_root = params['pascal_root']
        self.im_shape = params['im_shape']
        self.zoom_range= params['zoom_range']
        self.rotation_range = params['rotation_range']
        self.translation_range = params['translation_range']
        # get list of image indexes.
        list_file = params['split'] + '.txt'
        self.indexlist = [line.rstrip('\n') for line in open(
            osp.join(self.pascal_root, list_file))]
        self._cur = 0  # current image
        # this class does some simple data-manipulations
        self.transformer = SimpleTransformer()

        print "BatchLoader initialized with {} images".format(
            len(self.indexlist))

    def load_next_image(self,ds_transforms,augmentation_params):
        """
        Load the next image in a batch.
        """
        # Did we finish an epoch?
        if self._cur == len(self.indexlist):
            self._cur = 0
            shuffle(self.indexlist)
        #print(self._cur)
        # Load an image
        index = self.indexlist[self._cur]  # Get the image index
        image_file_name = index + '.png'
        imgs = np.asarray(Image.open(
            osp.join(self.pascal_root, 'unnormalized train images 256', image_file_name)))
        img=imgs;
        #print(img.shape)
        target_sizes=self.im_shape
        #print('1',target_sizes)
        #img = scipy.misc.imresize(img, (h,w))  # resize


        # do a simple horizontal flip as data augmentation
        #flip = np.random.choice(2)*2-1
        #im = im[:, ::flip, :]

        # Load and prepare ground truth
        #multilabel = np.zeros(20).astype(np.float32)
        #anns = load_pascal_annotation(index, self.pascal_root)
        #for label in anns['gt_classes']:
            # in the multilabel problem we don't care how MANY instances
            # there are of each class. Only if they are present.
            # The "-1" is b/c we are not interested in the background
            # class.
         #   multilabel[label - 1] = 1

        # Load an image

        label_file_name = index + '.png'
        lbl = np.asarray(Image.open(
            osp.join(self.pascal_root, 'labels_png_4ch', label_file_name)))
        #lbl = scipy.misc.imresize(lbl, (h,w))  # resize

        # do a simple horizontal flip as data augmentation
        [img_a,lbl_a] = perturb_and_dscrop(img,lbl,ds_transforms, augmentation_params, target_sizes)
        im=np.empty((3,256,256))
        im[0]=img_a;
        im[1]=img_a;
        im[2]=img_a;


        #print('self',self.transformer.preprocess(im).shape)
        """
        if self._cur<10 :
            #print (shear)
            fig=plt.figure(self._cur)
            a = fig.add_subplot(2, 2, 1)
            plt.imshow(img_a,aspect='auto')
            a = fig.add_subplot(2, 2, 2)
            plt.imshow(lbl_a,aspect='auto')
            a = fig.add_subplot(2, 2, 3)
            plt.imshow(img,aspect='auto')
            a = fig.add_subplot(2, 2, 4)
            plt.imshow(lbl,aspect='auto')
            plt.savefig(str(self._cur)+'s')
        """
        self._cur += 1
        return self.transformer.preprocess(im), lbl_a


#def load_pascal_annotation(index, pascal_root):
    """
    This code is borrowed from Ross Girshick's FAST-RCNN code
    (https://github.com/rbgirshick/fast-rcnn).
    It parses the PASCAL .xml metadata files.
    See publication for further details: (http://arxiv.org/abs/1504.08083).

    Thanks Ross!

    """
    #classes = ('__background__',  # always index 0
    #           'aeroplane', 'bicycle', 'bird', 'boat',
    #           'bottle', 'bus', 'car', 'cat', 'chair',
    #                     'cow', 'diningtable', 'dog', 'horse',
    #                     'motorbike', 'person', 'pottedplant',
    #                     'sheep', 'sofa', 'train', 'tvmonitor')
    #class_to_ind = dict(zip(classes, xrange(21)))

    #filename = osp.join(pascal_root, 'Annotations', index + '.xml')
    # print 'Loading: {}'.format(filename)

    #def get_data_from_tag(node, tag):
    #    return node.getElementsByTagName(tag)[0].childNodes[0].data

    #with open(filename) as f:
    #   data = minidom.parseString(f.read())

    #objs = data.getElementsByTagName('object')
    #num_objs = len(objs)

    #boxes = np.zeros((num_objs, 4), dtype=np.uint16)
    #gt_classes = np.zeros((num_objs), dtype=np.int32)
    #overlaps = np.zeros((num_objs, 21), dtype=np.float32)

    # Load object bounding boxes into a data frame.
    """
    for ix, obj in enumerate(objs):
        # Make pixel indexes 0-based
        x1 = float(get_data_from_tag(obj, 'xmin')) - 1
        y1 = float(get_data_from_tag(obj, 'ymin')) - 1
        x2 = float(get_data_from_tag(obj, 'xmax')) - 1
        y2 = float(get_data_from_tag(obj, 'ymax')) - 1
        cls = class_to_ind[
            str(get_data_from_tag(obj, "name")).lower().strip()]
        boxes[ix, :] = [x1, y1, x2, y2]
        gt_classes[ix] = cls
        overlaps[ix, cls] = 1.0

    overlaps = scipy.sparse.csr_matrix(overlaps)

    return {'boxes': boxes,
            'gt_classes': gt_classes,
            'gt_overlaps': overlaps,
            'flipped': False,
            'index': index}
    """

def check_params(params):
    """
    A utility function to check the parameters for the data layers.
    """
    assert 'split' in params.keys(
    ), 'Params must include split (train, val, or test).'

    required = ['batch_size', 'pascal_root']
    for r in required:
        assert r in params.keys(), 'Params must include {}'.format(r)


def print_info(name, params):
    """
    Output some info regarding the class
    """
    print "{} initialized for split: {}, with bs: {}.".format(
        name,
        params['split'],
        params['batch_size'])
        #params['im_shape'])


##Augmentation##

def fast_warp(img,lbl, tf, output_shape=(256,256), mode='constant'):
    """
    This wrapper function is about five times faster than skimage.transform.warp, for our use case.
    """

    m = tf._matrix
    #print('os',output_shape)
    img_wf = np.empty((output_shape), dtype='float32')
    lbl_wf = np.empty((output_shape), dtype='float32')
    #print(m)
    img_wf = (skimage.transform._warps_cy._warp_fast(img, m, output_shape=output_shape , mode=mode)).astype('float32')
    lbl_wf = (skimage.transform._warps_cy._warp_fast(lbl, m, output_shape=output_shape, mode=mode)).astype('float32')
    return img_wf, lbl_wf




## TRANSFORMATIONS ##

center_shift = np.array((256, 256)) / 2. - 0.5
tform_center = skimage.transform.SimilarityTransform(translation=-center_shift)
tform_uncenter = skimage.transform.SimilarityTransform(translation=center_shift)

def build_augmentation_transform(zoom=1.0, rotation=0, shear=0, translation=(0, 0)):
    #print(translation)
    tform_augment = skimage.transform.AffineTransform(scale=(1/zoom, 1/zoom), rotation=np.deg2rad(rotation), shear=np.deg2rad(shear), translation=translation)
    tform = tform_center + tform_augment + tform_uncenter # shift to center, augment, shift back (for the rotation/shearing)
    return tform


def build_ds_transform(ds_factor=1.0, orig_size=(256, 256), target_size=(256, 256), do_shift=True, subpixel_shift=False):
    """
    This version is a bit more 'correct', it mimics the skimage.transform.resize function.
    """
    rows, cols = orig_size
    trows, tcols = target_size
    col_scale = row_scale = ds_factor
    src_corners = np.array([[1, 1], [1, rows], [cols, rows]]) - 1
    dst_corners = np.zeros(src_corners.shape, dtype=np.double)
    # take into account that 0th pixel is at position (0.5, 0.5)
    dst_corners[:, 0] = col_scale * (src_corners[:, 0] + 0.5) - 0.5
    dst_corners[:, 1] = row_scale * (src_corners[:, 1] + 0.5) - 0.5

    tform_ds = skimage.transform.AffineTransform()
    tform_ds.estimate(src_corners, dst_corners)

    if do_shift:
        if subpixel_shift:
            # if this is true, we add an additional 'arbitrary' subpixel shift, which 'aligns'
            # the grid of the target image with the original image in such a way that the interpolation
            # is 'cleaner', i.e. groups of <ds_factor> pixels in the original image will map to
            # individual pixels in the resulting image.
            #
            # without this additional shift, and when the downsampling factor does not divide the image
            # size (like in the case of 424 and 3.0 for example), the grids will not be aligned, resulting
            # in 'smoother' looking images that lose more high frequency information.
            #
            # technically this additional shift is not 'correct' (we're not looking at the very center
            # of the image anymore), but it's always less than a pixel so it's not a big deal.
            #
            # in practice, we implement the subpixel shift by rounding down the orig_size to the
            # nearest multiple of the ds_factor. Of course, this only makes sense if the ds_factor
            # is an integer.

            cols = (cols // int(ds_factor)) * int(ds_factor)
            rows = (rows // int(ds_factor)) * int(ds_factor)
            # print "NEW ROWS, COLS: (%d,%d)" % (rows, cols)

        shift_x = cols / (2 * ds_factor) - tcols / 2.0
        shift_y = rows / (2 * ds_factor) - trows / 2.0
        tform_shift_ds = skimage.transform.SimilarityTransform(translation=(shift_x, shift_y))
        return tform_shift_ds + tform_ds
    else:
        return tform_ds



def random_perturbation_transform(augmentation_params):

    # random shift [-4, 4] - shift no longer needs to be integer!
    if augmentation_params['split']=='train':
        shift_x = np.random.uniform(-10,10)
        shift_y = np.random.uniform(-10,10)
        translation = (shift_x, shift_y)

        # random rotation [0, 360]
        rotation = np.random.uniform(-30,30) # there is no post-augmentation, so full rotations here!

        # random shear [0, 5]
        shear = np.random.uniform(-15,15)
    else:
        print('test')
        shift_x = np.random.uniform(0, 0)
        shift_y = np.random.uniform(0, 0)
        translation = (shift_x, shift_y)

        # random rotation [0, 360]
        rotation = np.random.uniform(0, 0)  # there is no post-augmentation, so full rotations here!

        # random shear [0, 5]
        shear = np.random.uniform(0, 0)
    #print(shear)
    # # flip
    ##if do_flip and (np.random.randint(2) > 0): # flip half of the time
    #    shear += 180
    #    rotation += 180
        # shear by 180 degrees is equivalent to rotation by 180 degrees + flip.
        # So after that we rotate it another 180 degrees to get just the flip.

    #random zoom [0.9, 1.1]
    #zoom = np.random.uniform(*zoom_range)
    log_zoom_range = [np.log(z) for z in augmentation_params['zoom_range']]
    zoom = np.exp(np.random.uniform(*log_zoom_range)) # for a zoom factor this sampling approach makes more sense.
    # the range should be multiplicatively symmetric, so [1/1.1, 1.1] instead of [0.9, 1.1] makes more sense.

    return build_augmentation_transform(zoom, rotation, shear, translation)


def perturb_and_dscrop(img, lbl, ds_transforms, augmentation_params, target_sizes=None):
    if target_sizes is None: # default to (53,53) for backwards compatibility
        target_sizes = [(256, 256) for _ in xrange(len(ds_transforms))]

    tform_augment = random_perturbation_transform(augmentation_params)
    # return [skimage.transform.warp(img, tform_ds + tform_augment, output_shape=target_size, mode='reflect').astype('float32') for tform_ds in ds_transforms]

    result = []
    # print('tsize',target_sizes,'ds',ds_transforms)
    for tform_ds, target_size in zip(ds_transforms, target_sizes):
        #print('tsize', target_size)
        result=((fast_warp(img, lbl, tform_ds + tform_augment, output_shape=(256,256), mode='constant')))
    return result




