import subprocess
import platform
import copy

import scipy.misc
import scipy.io
import sys
import os
import numpy as np
import Image
import matplotlib.pyplot as plt
import h5py
import caffe
#import caffe.draw
import time
import Dice_4ch

import matplotlib.image as mpimg
data=np.zeros((449,256,256));
label=np.zeros((449,256,256));
for ii in range (1,450):
    filename="/home/exx/Desktop/4ch Echo Segmentation/Data/Images/"+str(ii+946)+".png"
    labelname="/home/exx/Desktop/4ch Echo Segmentation/Data/Labels/"+str(ii+946)+".png"
    data[ii-1,:,:] = Image.open(filename);
    label[ii-1,:,:] =Image.open(labelname);
#np_label = np.array(label)
#np_data = np.array(data)
print('Shape of the array dataset_1: \n', data.shape)
    # data=Image.open("/home/exx/Pictures/images_jpg_256/1.jpg")
    # np_data=np.array(data)

weights = '/home/exx/Desktop/4ch Echo Segmentation/pascalsemanticseg_4ch_256_allpng_Nest2_iter_90000.caffemodel'
deploy_file1 = '/home/exx/Desktop/4ch Echo Segmentation/deploysemanticseg_fcn32.prototxt'

# model_file2= '/media/exx/22d9dd91-2bea-4cea-b04a-647943fbfc73/caffe-examples/semanticseg/newsemanticseg-_iter_746.caffemodel'
# deploy_file2='/media/exx/22d9dd91-2bea-4cea-b04a-647943fbfc73/caffe-examples/semanticseg/2_deploy.prototxt'


net1 = caffe.Net(deploy_file1, weights, caffe.TEST)
# net2 = caffe.Net(deploy_file2,model_file2,caffe.TEST)
inputt = np.zeros((449, 3, 256, 256));
input = np.zeros((1, 3, 256, 256));
ii = 448
result = np.zeros((449, 256, 256))
for jj in range (0,3):
    inputt[:, jj, :, :] = data[:, :, :];

dice_metric = np.zeros((449,4));
# inputs=np.random.rand(1,3,256,256)
for kkk in range (0,449):
    print(kkk)
    input[0] = inputt[kkk,:, :, :];
    net1.blobs['data'].data[...] = input
    net1.forward()
    outt1 = net1.blobs['scoresoft'].data[...]
    result[kkk, :, :] = outt1[0].argmax(axis=0);

for jjj in range(0, 449):
    # segmentation
    seg = result[jjj, :, :];
    # ground truth
    gt = label[jjj,:,:];

    #dice_metric[jjj,:]=Dice_4ch(auto=seg,manual=gt)

    for k in range(1, 5):
        autoseg=np.sign(-1*((np.array(seg)-k)*(np.array(seg)-k)))+1
        manseg=np.sign(-1*((np.array(gt)-k)*(np.array(gt)-k)))+1
        dice_metric[jjj,k-1] = np.sum(np.array(autoseg)*np.array(manseg)) * 2.0 / (np.sum(autoseg) + np.sum(manseg))


    fig = plt.figure(jjj)
    a = fig.add_subplot(1, 3, 1)
    plt.imshow(result[jjj, :, :], cmap="gray")

    a.set_title('Auto result')
    a = fig.add_subplot(1, 3, 2)
    plt.imshow(label[jjj, :, :], cmap="gray")
    a.set_title('Ground truth')
    a = fig.add_subplot(1, 3, 3)
    plt.imshow(inputt[jjj, 0, :, :], cmap="gray")
    a.set_title('Image')
    """
    a = fig.add_subplot(1, 4, 4)
    plt.imshow(rv, cmap="gray")
    a.set_title('testrv')
    """
    plt.savefig(str(jjj) + 'res2_4ch_90k_oct')
    plt.close()

print(dice,dice.shape)
print 'Dice LV similarity score is {}'.format(np.mean(dice_metric[:,0]))
print 'Dice RV similarity score is {}'.format(np.mean(dice_metric[:,1]))
print 'Dice LA similarity score is {}'.format(np.mean(dice_metric[:,2]))
print 'Dice RA similarity score is {}'.format(np.mean(dice_metric[:,3]))
scipy.io.savemat('4ch_Nest2_256_90k_oct.mat', mdict={'autoresult': result, 'dice': dice_metric,'image':inputt, 'labels': label})

"""
print(ii)
ii = ii + 1
scipy.io.savemat('arrdata.mat', mdict={'arr': arr})

print ('inputt shape', inputt.shape, inputt[0, 0, 0, 0:10])
print ('inputs shape', inputs.shape, inputs[0, 0, 0, 0:10])

net11 = caffe.Net(deploy_file1, model_file1, caffe.TEST)
net11.blobs['data'].data[...] = inputs
net11.forward()
outt11 = net11.blobs['scoresoft'].data[...]
print('out11:\n', outt11)

print('out1:\n', outt1)
diff = outt1 - outt11
print('diff:\n', diff, diff.shape)

# net2.blobs['data'].data[...] = inputt
# net2.forward()

# print('Shape of the array out: \n', len(out1))

# print(out2)

#print('out: {0}'.format(out))
#relu1=np.array(out1['conv1'])
#relu11=np.array(out2['conv1'])
#relu11=net2.blobs['pool1'].data[...]
fc7=net1.blobs['score1'].data[...]
print('Shape of the array deconv: \n', fc7.shape)
print(fc7[0,...,1:10,0])

fc7_rnd=net11.blobs['score1'].data[...]
print('Shape of the array crp rand: \n', fc7_rnd.shape)
print(fc7_rnd[0,...,1:10,0])
imgplot = plt.imshow(inputs[0,0])

#print('Shape of the array pool11: \n', relu11[0,0].shape)
#print(relu11[0,...,1,1])
#imgplot = plt.imshow(relu11[0,0])

#scr=net.blobs['pool1'].data[...]
#print('Shape of the array pool1: \n', scr.shape)
#print(scr[0])

#scr2=net.blobs['deconv4'].data[...]
#print('Shape of the array scr2: \n', scr2.shape)
#print(scr2[0])

scipy.misc.imsave('out1.jpg', fc7[0,0])
#scipy.misc.imsave('outfile1.jpg', scr[0])

#print('max img: \t', np.amax(img[...]))
#print('min img: \t', np.amin(img[...]))


#print('max scr: \t', np.amax(scr[...]))
#print('min scr: \t', np.amin(scr[...]))


#scipy.io.savemat('images.mat', mdict={'images': scr})
"""