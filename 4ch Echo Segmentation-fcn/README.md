This is the reference implementation of the models and codes for the fully convolutional networks (FCNs) for 4-chamber segmentation for Echo Data.


1- Manually annotation: use "Maskcreator.m" to read dicom files and manually annotate each chamber
	* Data type for both images and labels should be Uint8. Make sure they are in correct format.
2- Make sure you have caffe and Pycagge successfully installed.
3- In this project a costumized python data layer was used, in order to have real-time data augmentation ("pascal_multilabel_datalayers.py")
	* make sure you change the location for your data and also change the augmentation parameters as needed
4- The network architecture can be found in  "test_train_voc_fcn32_semseg_echo.prototxt"
5- For network solver and optimization there ar 2 files : "solve.py", "newsolver.prototxt"
	* change hyperparameters as desired 
6- Run "solve.py" to start training
7- Visualization : use "4ch_netreader.py", add directory address for the test data 
	* last section of the code calculates Dice metrics for all 4 chamber and save automated results and dice metrics as a mat file

