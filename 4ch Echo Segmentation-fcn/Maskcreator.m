clear all
addpath ('C:\Users\Arghavan\Desktop\altmany-export_fig-e1b8666');


filename='726719154';
I=dicomread(filename);
size=size(I);
n=floor(size(4)/5);
for ii=11
    image=I(:,:,1,5*ii);
    fig=imshow(image);
%     %LV
    lv=imfreehand(gca,'closed',false);
    setColor(lv,'y'); 
    LVmask=createMask(lv);
    labellv_xy=contourc(double(LVmask));
    
    lblIDlv=['LV',filename, num2str(5*ii), '.txt'];
    lblIDlvmat=['LV',filename, num2str(5*ii), '.mat'];
    save(lblIDlvmat,'LVmask');
    f =fopen(lblIDlv,'wt');
    fprintf (f,'%f  %f  \n',labellv_xy);
    fclose('all');
    % RV
    rv=imfreehand(gca,'closed',false);
    setColor(rv,'r'); 
    RVmask=createMask(rv);
    labelrv_xy=contourc(double(RVmask));
    
    lblIDrv=['RV',filename, num2str(5*ii), '.txt'];
    lblIDrvmat=['RV',filename, num2str(5*ii), '.mat'];
    save(lblIDrvmat,'RVmask');
    ff=fopen(lblIDrv,'wt');
    fprintf (ff,'%f  %f   \n',labelrv_xy);
    fclose('all');
    
    %LA
    la=imfreehand(gca,'closed',false);
    setColor(la,'b'); 
    LAmask=createMask(la);
    labella_xy=contourc(double(LAmask));
    
    lblIDla=['LA',filename, num2str(5*ii), '.txt'];
    lblIDlamat=['LA',filename, num2str(5*ii), '.mat'];
    save(lblIDlamat,'LAmask');
    fff =fopen(lblIDla,'wt');
    fprintf (fff,'%f  %f  \n',labella_xy);
    fclose('all');
    
    %RA
    ra=imfreehand(gca,'closed',false);
    setColor(ra,'m'); 
    RAmask=createMask(ra);
    labelra_xy=contourc(double(RAmask));
    
    lblIDra=['RA',filename, num2str(5*ii), '.txt'];
    lblIDramat=['RA',filename, num2str(5*ii), '.mat'];
    save(lblIDramat,'RAmask');
    ffff=fopen(lblIDra,'wt');
    fprintf (ffff,'%f  %f  \n',labelra_xy);
    fclose('all');
    
    ImageID=['Img',filename, num2str(5*ii),'.mat'];
    save(ImageID,'image');
    
  
   export_fig(['Img',filename, num2str(5*ii),'A'])
 
    clear labelrv_xy labellv_xy RVmask LVmask ;
 
end
